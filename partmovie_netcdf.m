x = ncread('output_0.nc','X');
y = ncread('output_0.nc','Y');

for ii=0:5:150
    %     eta = ncread(['output_' num2str(ii) '.nc'],'eta');
    part_x = ncread(['output_' num2str(ii) '.nc'],'particle_x_position');
    part_y = ncread(['output_' num2str(ii) '.nc'],'particle_y_position');
    vort = ncread(['output_' num2str(ii) '.nc'],'vorticity');
    u = ncread(['output_' num2str(ii) '.nc'],'u');
    v = ncread(['output_' num2str(ii) '.nc'],'v');
    clf
    pcolor(x,y,vort'),shading flat,colormap jet
    hold
    plot(part_x,part_y,'.k')
    title(ii)
    drawnow
end