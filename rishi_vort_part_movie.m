close all;clear all;
x = ncread('output_0.nc','X');
y = ncread('output_0.nc','Y');
endout=150;
part_x = ncread(['output_' num2str(0) '.nc'],'particle_x_position');
part_y = ncread(['output_' num2str(0) '.nc'],'particle_y_position');
vort = ncread(['output_' num2str(0) '.nc'],'vorticity');
subplot(2,2,1)
pcolor(x,y,vort),shading flat
caxis([-1 1]*4e-5)
axis equal
%axis([2 4 7 9]*1e4)
subplot(2,2,3)
pcolor(x,y,vort),shading flat
caxis([-1 1]*4e-5)
hold
plot(part_x,part_y,'.k')
axis equal
%axis([2 4 7 9]*1e4)
for ii=1:2
    part_x = ncread(['output_' num2str(ii) '.nc'],'particle x position');
    part_y = ncread(['output_' num2str(ii) '.nc'],'particle y position');
    vort = ncread(['output_' num2str(ii) '.nc'],'vorticity');
    subplot(2,2,2)
    pcolor(x,y,vort),shading flat
    caxis([-1 1]*4e-5)
    axis equal
    %axis([2 4 7 9]*1e4)
    subplot(2,2,4)
    pcolor(x,y,vort),shading flat
    caxis([-1 1]*4e-5)
    hold on
    plot(part_x,part_y,'.k')
    axis equal
    %axis([2 4 7 9]*1e4)
    hold off
    drawnow
end