
# Compiler and options
# Need gnu99 standard to use popen
# Links FFTW and Math
CXX = nvcc
CFLAGS = -Wno-deprecated-gpu-targets  -I/usr/include/hdf5/openmpi -I/usr/include/openmpi -D_FORCE_INLINES
CUFLAGS =
LFLAGS = -L/usr/lib/openmpi -L/opt/hdf5-1.8.17/hdf5/lib -lmpi -lnetcdf -lhdf5_hl -lhdf5 -lz -lm -lcufftw -lcufft -lcudart -lcurl 

DEBUG=TRUE
DEBUG_FLAGS= -G -g -Xcompiler -rdynamic

OPT=FALSE
OPT_FLAGS = -O2

ifeq ($(DEBUG),TRUE)
    CFLAGS += $(DEBUG_FLAGS) -D DEBUG
endif

ifeq ($(OPT),TRUE)
    CFLAGS += $(OPT_FLAGS)
endif

###
all: default
.PHONY: clean all
default: SW_main.x

SW_main.x: SW_main.o Shallow_Water_Eq.o Variable.o Particle.o cuda_kernels.o
	$(CXX) $(CFLAGS) $^ -o $@ $(LFLAGS)

SW_main.o: SW_main.cu Shallow_Water_Eq.h Variable.h Particle.h cuda_kernels.h
	$(CXX) $(CFLAGS) -c SW_main.cu $(LFLAGS)

Shallow_Water_Eq.o: Shallow_Water_Eq.cu Shallow_Water_Eq.h cuda_kernels.h Variable.h
	$(CXX) $(CFLAGS) -c Shallow_Water_Eq.cu $(LFLAGS)

Variable.o: Variable.cu Variable.h cuda_kernels.h
	$(CXX) $(CFLAGS) -c Variable.cu $(LFLAGS)

Particle.o: Particle.cu Particle.h cuda_kernels.h
	$(CXX) $(CFLAGS) -c Particle.cu $(LFLAGS)

cuda_kernels.o: cuda_kernels.cu cuda_kernels.h
	$(CXX) $(CFLAGS) -c cuda_kernels.cu $(LFLAGS)

clean:
	rm -f SW_main.x *.o *.nc

