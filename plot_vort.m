close all;clear all;
x = ncread('output_0.nc','X');
y = ncread('output_0.nc','Y');
numgroups=20;
myincrement=160000/numgroups
aa=colormap(parula(numgroups/2));
aa=[aa;flipud(aa)];
for ii=0:1:800
clf
%vort = ncread(['output_' num2str(ii) '.nc'],'vorticity');
%pcolor(x,y,vort'),shading flat,caxis([-1 1]*0.5)
part_x = ncread(['output_' num2str(ii) '.nc'],'particle x position');
part_y = ncread(['output_' num2str(ii) '.nc'],'particle y position');
hold
for jj=1:numgroups
 h=plot(part_x((jj-1)*myincrement+1:jj*myincrement),part_y((jj-1)*myincrement+1:jj*myincrement),'.')
 set(h,'MarkerSize',4)
 set(h,'Color',aa(jj,:))
% set(h,'Color',[1,1,1]-[1 1 1]*jj/numgroups)
end
 title(ii)
 drawnow
end