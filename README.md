This code solves the Shallow Water Equations and tracks Lagrangian particles.
It uses CUDA to solve both the equations and the particles as such requires a
CUDA compatible GPU to run. The makefile assumes that the Nvidia compiler is
installed in the default path and can be called with "nvcc". The code uses 
and creates NETCDF files which contain all the information nescessary to run
(you can start from any .nc file). Since NETCDF is used it must be installed
and available.

Currently this code solves the equations using a low-memory RK2 for timestepping
and spectral methods for space.
The particles are interpolated using a cubic interpolation and timestepped using
symplectic Euler.

To build:
make

To make initial conditions run the python script (there are different ones
depending on the initial conditions you want), for example:
python3 config_jet.py

To run the code:
./SW_main.x

To clean up:
make clean
(Will also remove any .nc files that are created by running the code)